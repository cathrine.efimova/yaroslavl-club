document.addEventListener('DOMContentLoaded', () => {
    let previews = document.querySelectorAll('.itc-preview'); /*выбираем все превьюхи*/
    let bigSlides = document.querySelector('.itc-slider-1').querySelectorAll('.itc-slider-item'); /*выбираем все превьюхи*/

    RC_SMALL_BOOKINGS_WIDGET.init('02326ba3d1b827e05cc34f695579d82c'); /*инициализация формы*/
    document.querySelectorAll('.itc-slider').forEach((el) => { /*инициализация всех слайдеров*/
        ItcSlider.getOrCreateInstance(el, {
            loop: false,
        });
    });
    for (let i = 0; i < previews.length; i++) {/*вешаем обработчик на все превьюхи*/
        previews[i].addEventListener("click", (e) => prevClickHandler(e));
    }
    /*ловим события изменения в большой карусели*/
    let target = document.querySelector('.itc-slider-1 .itc-slider-item');
    const config = {
        attributes: true,
    };
    const observer = new MutationObserver((e) => bigSlidesClickHandler(e));
    observer.observe(target, config);
});

function prevClickHandler(e) {/*обработка клика по превьюхе*/
    let bigSlider = document.querySelector('.itc-slider-1'); //большой слайдер
    goToSlide(e?.target, bigSlider); /*переход к большому слайду*/
}

function bigSlidesClickHandler(e) {/*обработка клика по большому слайду*/
    let current = e.target ? e.target.closest('.itc-slider-items').querySelector('.itc-slider-item-active') : e[0].target.closest('.itc-slider-items').querySelector('.itc-slider-item-active');
    if (current) {
        let previewSlider = document.querySelector('.itc-slider-3'); //слайдер превьюх
        goToSlide(current, previewSlider); /*переход к превьюхе*/
        currentPrevSlide(current.getAttribute('data-slide-to')); /*выбор активной превьюхи*/
    }
}

function goToSlide(e, sliderElement) {/*переход к большому слайду*/
    let slider = ItcSlider.getInstance(sliderElement); //получение экземпляра класса ItcSlider
    let bigSlide = e.getAttribute('data-slide-to')
    slider.slideTo(bigSlide);
}
function currentPrevSlide(current) {
    let previews = document.querySelectorAll('.itc-preview'); //превьюхи
    let sliderElem = document.querySelector('.itc-slider-3'); //этот слайдер
    let slider = ItcSlider.getInstance(sliderElem); //получение экземпляра класса ItcSlider
    let prevItemsActive = document.querySelectorAll(".itc-preview.itc-slider-item-active"); //все видимые слайды
    let lastPrevItem = prevItemsActive[prevItemsActive.length - 1]; //последний видимый слайд
    let firstPrevItem = prevItemsActive[0]; //первый видимый слайд

    for (let i = 0; i < previews.length; i++) { /*убираем класс current у всех превьюх*/
        previews[i].classList.remove('current');
    }
    previews[current].classList.add('current'); /*добавляем класс current для выбранного слайда*/
    if (previews[current] === lastPrevItem) { /*клик по последнему видимому элменту*/
        slider.slideNext();
    }
    if (previews[current] === firstPrevItem) { /*клик по первому видимому элменту*/
        slider.slidePrev();
    }
}
